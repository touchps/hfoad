import com.java.entities.DogDoor;
import com.java.entities.Remote;

public class DogDoorSimulator {
   public static void main(String[] args) {
      DogDoor door = new DogDoor();
      Remote remote = new Remote(door);

      System.out.println("Fido barks to go outside");
      remote.pressButton();

      System.out.println("\nFido has gone outside");

      System.out.println("\nFido's all done");

      try {
         Thread.currentThread().sleep(10000);
      } catch (InterruptedException ex) {
         ex.printStackTrace();
      }

      System.out.println("\nFido's is stuck outside");

      System.out.println("\nFido starts barking");

      System.out.println("\nTodd/Gina press button on RC again");
      remote.pressButton();
      System.out.println("\nFido's back inside");
   }
}
