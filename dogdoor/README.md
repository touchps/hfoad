### Compile
```bash
javac -cp "./:./jars" -d cls DogDoorSimulator.java
```

### Run
```bash
java -cp "./cls:./jars" DogDoorSimulator
```

### Iteration 1: Simple dog door with boolean door state

Initial Requirements
- remote managed dog door to let the dog out and in.
- upto specification.
- not quite usable practically, has practial issues as well.
- other animals come in if forgot to close the door.

### Iteration 2: Door not working as customer expected

Problems
- other animals getting in
- had to wait for dog to come in before you close the door
- dog too tall for the door

Talk to customer to get additional info on requirement.  

Requirement List 2.0
--------------------
1. Dog door must be 12 ft tall.
2. RC button should open the door if closed and close if open.
3. Once opened, door should close automatically if not already closed.

> Tip: always ask more questions to anticipate and additional stuff customer may need and have not asked.

Dogdoor v2.0
------------
1. Fido barks to be let out
2. Todd/Gina hears Fido barking
3. Todd/Gina press button on RC
4. The dogdoor opens
5. Fido goes outside
6. Fido does his business
7. Fido goes back inside
8. The door shuts automatically


### Iteration 3: Alternate path

Dogdoor v2.1
------------

Plan for things that can go wrong
---------------------------------
1. what if dog scratches instead of bark to open the door.
2. what if todd/gina not in home when fido barks.
3. what if fido barks in mid of the day (not required to go outside).
4. what if fido stuck outside barking and door automatically closes.

Alternate path to handle system problems
----------------------------------------

1. Fido barks to be let out
2. Todd/Gina hears Fido barking
3. Todd/Gina press button on RC
4. The dogdoor opens
5. Fido goes outside
6. Fido does his business
   1. The door closes automatically
   2. Fido barks to be let back inside
   3. Todd/Gina hears Fido barking
   4. Todd/Gina presses button on RC
   5. Dog door opens (again) 
7. Fido goes back inside
8. The door shuts automatically

Use Case
--------
Captures potential requirements of a new system or software change. 
- Each use case provies one or more `scenarios`.
- Scenarios convey how system should `interact` with end user/another system to achieve `specific goal`.

Each use case should have
- clear value
- start and stop points (indicating conditions)
- external initiator

> Cross check with requirements to validate completeness of use case

### Iteration 5: Success/happy path with timeout.

Code changes with simulator covering the use case path when dog completes the outing and gets before door timeout.
- we only add code to cover 3rd point of requirement 2.0
- others are already covered in existing code.

### Iteration 6: Alternate path with timeout

Case where dog doesn't get back before door timeout, hence the door is closed.

### Iteration 7: New Requirements

**Kristen and Bitsie dog**

Kristen wants to lock doors using a keypad to ignore constant nudging by his dog.

Requirement List: 2.1
1. The keypad must accept a 4-digit lock code
2. The keypad must be able to lock the dog door and all the windows.
3. The keypad must be able to unlock and the dog door and all the windows in the house.

Use Case
1. Kristen enters a code on keypad.
2. The dog door and all windows in the house lock.

> Requirement and use case impact each other, bad use case can cause bad requirement.

In this case dog door and windows are always open otherwise when unlocked and dog can move inside/outside freely.

**John's dog**

John wants that Door should automatically close when dog goes outside. He wants to prevent muddy dog to get back in the house.
But we do need a requirement list. 

Requirement List 2.2
1. (Somehow) the dog door opens. (We need more input from John as its not clear).
2. Tex goes outside.
3. The dog door closes automatically.
4. Tex does this business.
  1. Tex gets muddy (alternate path, inferred, sometimex tex doesn't get muddy)
  2. John cleans Tex up
5. John presses the button on RC.
6. Dog door opens.
7. Tex comes inside.
8. The dog door closes automcatically


Requrement List 2.2
1. Dog door must detect scratching from a dog.
2. The door should be able to open on a command

**Holly and Bruce's dog**

Because of constant bark it cannot be used as input, so open the door when its scratched from paws.
Note that she never talks how the door should be closed.

Requirement List: 2.3
1. Bruce scratches the door
2. The dog door opens
3. Bruce goes outside
4. The dog door closes automatically (Inferred)
5. Bruce does his business
6. Bruce scratches the door again
7. The Dog door opens up again
8. Bruce comes inside.
9. The dog door closes automatically