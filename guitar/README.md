## Compile Command

For generating clases in separate folder while preserving folder structure
```
javac -cp . -d cls/ FindGuitarTester.java
```

For generating classes all over
```
javac -cp . FindGuitarTester.java
```

## Run Command
```
java -cp ./cls FindGuitarTester
```

## Notes

### Iteration 1: Search not working

Problems ?
- too much string, consider objects/constants
- client wants multi options to select
- too much coupling b/n inventory and guitar

Principles

Part of good software
- customer should get what he wants (happp)
- well coded,easy to reuse/extend (OOP, apply basic OO principles)
- design guru, tried and tested patterns, ready to use for years to come

### Iteration 2: Fix search

Fix
- don't create problems as part of solving problems. So introduced enum types rather than using lowercase comparison for all strings
- enumarated types ensure only valid values and accepted.
- be smart about fixing things rather than come back and fix later.

### Iteation 3: Match what customer wants

Motivation
- ensure the basic functionality works before moving on to strive for OO principles and maintainebale, usable design


### Iteration 4: Break Guitar into GuitarSpec further to encapsulate search properties

Motivation
- use textual description to ensure deisgn lines up with intended functionality
  - work as name indicates
  - single concept
  - unused property dead giveaway for more than 1 responsibility

Problem
- Guitar search in inventory is using guitar object means serial number is not needed.
- while useful, this means guitar is being used by more than 1 purpose.
- If we define separate spec, a part of it overlaps/duplicates guitar properties
  - future changes in properties may end up affecting both parts


Better way is to encapsulate part of guitar properties into seperate GuitarSpec, that way we can use spec for search while keeping the 
part that never changes like serial number within guitar only.
- encapsulation provides reusability as well.


### Iteration 5: Delegate creation and equality check to guitar spec
- for every new attribute in guitar spec, we change both guitar and inventory.
  - inventory for since we create on specific guitar attributes, comparison as well (knows too much about guitar)
  - guitar since it accept all properties at attribute level

Use Delegation
- forward operation to another object on behalf of me
- lets object worry about its own functionality

isolate guitarspec from rest of the code (guitar and inventory). Guitar and Inventory should delegate spec create and match to GuitarSpec.


### Iteration 6: Add new attribute to guitarspec
- no need of change in inventory and guitar because they delegate
- mostly the benefit of about refactoring based on encapsulation