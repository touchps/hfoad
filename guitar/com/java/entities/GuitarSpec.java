package com.java.entities;

public class GuitarSpec {
   private Builder builder;
   private String model;
   private Type type;
   private int numStrings;
   private Wood backWood;
   private Wood topWood;
   public GuitarSpec(Builder builder, String model, Type type, int numStrings, Wood backWood, Wood topWood) {
      this.builder = builder;
      this.model = model;
      this.type = type;
      this.numStrings = numStrings;
      this.backWood = backWood;
      this.topWood = topWood;
   }
   public Builder getBuilder() {
      return builder;
   }
   public String getModel() {
      return model;
   }
   public Type getType() {
      return type;
   }

   public int getNumStrings() {
      return numStrings;
   }

   public Wood getBackWood() {
      return backWood;
   }
   public Wood getTopWood() {
      return topWood;
   }

   public boolean matches(GuitarSpec otherSpec) {
      if (this.getBuilder() != otherSpec.getBuilder()) return false;

      String model = otherSpec.getModel().toLowerCase();
      if (model != null && (!model.equals("")) && (!model.equals(otherSpec.getModel().toLowerCase()))) return false;

      if (this.getType() != otherSpec.getType()) return false;

      if (this.getBackWood() != otherSpec.getBackWood()) return false;

      if (this.getTopWood() != otherSpec.getTopWood()) return false;

      return true;
   }
   
}
