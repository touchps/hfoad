package com.java.entities;

public class Guitar {
   private String serialNumber, model;
   private GuitarSpec spec;
   Builder builder;
   Type type;
   Wood backWood, topWood;
   private double price;

   public Guitar(
      String serialNumber,
      double price,
      GuitarSpec spec
   ) {
      this.serialNumber = serialNumber;
      this.price = price;
      this.spec = spec;
   }

   public String getSerialNumber() {
      return serialNumber;
   }

   public double getPrice() {
      return price;
   }

   public void setPrice(float newPrice) {
      this.price = newPrice;
   }

   public GuitarSpec getSpec() {
      return spec;
   }
   
}
